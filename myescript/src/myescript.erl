-module(myescript).

-import(lib_parallel, [pmap/3]).
%% API exports
-export([main/1]).

%%====================================================================
%% API functions
%%====================================================================

%% escript Entry point
main(Args) ->
    % io:format("Args: ~p~n", [Args]),
    % pmap(fun(X) -> io:format("~.10B~n", [X]) end, [1, 2, 3, 4, 5], 5),

    Out = docopt:docopt2(get_help(), Args),
    io:format("~s~n", [to_json(Out)]),
    % try docopt:docopt(get_help(), string:join(Args, " ")) of
    %         Out -> io:format("~s~n", [to_json(Out)])
    %     catch
    %       _:_ -> io:format("~s~n", [get_help()])
    % end,
    erlang:halt(0).

get_help() ->
"Find

Usage:
  find [--path=<start-path>] <pattern>... [--exec=<exec-program>] [--regex=<regex-match>]
                                          [--debug=<debug-mode>]

Options:
  pattern                   such as *.c *.py
  -p --path=<start-path>    find start from startdir(default os.curdir)
  -e --exec=<exec-program>  exec other command by pipe like -exec \"xxx %s \", %s:file-name
  -r --regex=<regex-match>  use regex match
  -d --debug=<debug-mode>   turn on debug mode".

to_json(KVs) ->
  StrKVs = lists:map(fun({Key, Value}) ->
                             io_lib:format("~p: ~s", [Key, format_value(Value)])
                     end, KVs),
  io_lib:format("{~s}", [string:join(StrKVs,", ")]).

format_value(undefined) -> null;
format_value(V)         -> io_lib:format("~p", [V]).
%%====================================================================
%% Internal functions
%%====================================================================
